#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <chrono>

void mergeVectors(std::vector<std::string>& names, std::vector<std::string>& sorted_names, size_t start, size_t mid_point, size_t end) {
    // This function compares the names in the "sliced" vectors, and sorts both slices
    // alphabetically.
    size_t i = start;
    size_t j = mid_point;
    for (size_t k = start; k < end; ++k) {
        if (i < mid_point && (j >= end || sorted_names[i] <= sorted_names[j])) {
            names[k] = sorted_names[i];
            ++i;
        } else {
            names[k] = sorted_names[j];
            ++j;
        }
    }
}

void mergeSort(std::vector<std::string>& sorted_names, std::vector<std::string>& names, size_t start, size_t end) {
    // This function "slices" the vectors in half recursively, until only one or two items remain in each slice
    if (end - start <= 1) {
        return;
    }
    size_t mid_point = (end + start) / 2;

    mergeSort(names, sorted_names, start, mid_point);
    mergeSort(names, sorted_names, mid_point, end);

    // When the "slices" both contains one or two elements,
    // mergeVectors is called to "join" the two vectors, sorting them simultaneously. 
    mergeVectors(sorted_names, names, start, mid_point, end);
}

void writeOutfile(std::vector<std::string>& names) {
    std::ofstream outfile;
    outfile.open("../data/sorted_names.txt");
    for (size_t i = 0; i < names.size(); ++i) {
        outfile << names[i] << "\n";
    }
    outfile.close();
}

int main() {
    std::vector<std::string> names, sorted_names;
    std::ifstream input("../data/names.txt");
    std::string name;
    // reading names one by one to vector
    while (input >> name) {
        names.push_back(name);
    }
    // making a copy of the vector for merge sort
    sorted_names = names;
    auto start_time = std::chrono::steady_clock::now();
    // creating indices for merge sort
    size_t start = 0, end = names.size();
    mergeSort(sorted_names, names, start, end);
    auto end_time = std::chrono::steady_clock::now();
    std::chrono::duration<double> seconds = end_time - start_time;
    std::cout << "Sorted " << names.size() << " names in " << seconds.count() << " seconds.";
    // writing sorted_names array to file
    writeOutfile(sorted_names);
    return 0;
}