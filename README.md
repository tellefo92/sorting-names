# Sorting names
This program takes a .txt file [names.txt](https://gitlab.com/tellefo92/sorting-names/-/blob/main/data/names.txt) from the [data](https://gitlab.com/tellefo92/sorting-names/-/tree/main/data), sorts the names alphabetically, and then writes the sorted names to [sorted_names.txt](https://gitlab.com/tellefo92/sorting-names/-/blob/main/data/sorted_names.txt).

## Requirements
Requires `cmake` and `cpp` <br />
data/names.txt must contain a single name (without whitespace) on each line.

## Usage
Clone the repository using
```sh
$ git clone https://gitlab.com/tellefo92/sorting-names.git
```
then navigate into the folder using
```sh
$ cd sorting-names
```
Uses Cmake to build:
```sh
# Create build directory and move into it
$ mkdir -p build && cd build
# Generate makefile using Cmake - default is release
$ cmake ../
# Generate makefile for debug build
$ cmake ../ -DCMAKE_BUILD_TYPE=debug      
# Compile the project
$ make
#Run the compiled binary
$ ./main
```

## Maintainers
Tellef Østereng | [@tellefo92](https://gitlab.com/tellefo92)
